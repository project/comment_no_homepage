CONTENTS OF THIS FILE
---------------------

* Overview
* Features
* Requirements
* Installation
* Known problems
* Version history
* Future plans
* Similar projects
* Credits

OVERVIEW
--------

Drupal 7 core's comment module adds a homepage field. But you might not want it.
But it's non-trivial to remove it, because each node has a different comment
form name, making hook_form_alter()s difficult.

FEATURES
--------

* Simply denies access to the homepage field for all users.
* There are no settings to change: simply turn on the module and the field is
  gone.
* The list of comment form names is cached so performance doesn't take too big
  of a hit.
* Tested with SimpleTest so it's clear when/if a change to the module breaks
  it's core functionality.

REQUIREMENTS
------------

Core's comment module.

INSTALLATION
------------

1. Download, install and enable the comment_no_homepage project.

   See https://drupal.org/node/895232 for further information.

KNOWN PROBLEMS
--------------

I don't know of any problems at this time, so if you find one, please let me
know by adding an issue!

VERSION HISTORY
---------------

The 1.x branch is the current stable branch.

FUTURE PLANS
------------

I don't currently have any future plans for this module: I just wanted a simple
way to turn off the homepage field that I could use on multiple sites. That
being said, I'm open to patches so long as they don't over-complicate the
module.

SIMILAR PROJECTS
----------------

I don't know of any similar projects at this time, so if you find one, please
let me know by adding an issue!

CREDITS
-------

Concept and coding by mparker17. Sponsored by OpenConcept Consulting
(http://openconcept.ca/).
